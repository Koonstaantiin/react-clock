import React from 'react';
import { createStore } from 'redux';
import { render } from 'react-dom';
import HotelClock from './HotelClock.jsx';
import Moment from "moment/moment";

const offsetDate = (state = 0, action) => {
    // if (action.type === 'SETNEWDATE') {
    return Moment().toDate();
};

const clockStore = createStore(offsetDate);

let updateDate = () => clockStore.dispatch({type: "SETNEWDATE"});

class App extends React.Component {
    render () {
        return  (
            <React.Fragment>
                <HotelClock clockStore={clockStore}/>
                <HotelClock clockStore={clockStore} timeOffset="5"/>
                <HotelClock clockStore={clockStore} timeOffset="-3"/>
            </React.Fragment>
        )
    }
}

render(<App/>, document.getElementById('clockApp'));

updateDate();

setInterval(updateDate, 1000);