import React from 'react';
import ReactDOM from 'react-dom';
import Moment from 'moment';

class HotelClock extends React.Component {

    constructor(props) {
        super(props);
        this.state = { date: this.props.clockStore.getState() };
        this.props.timeOffset = ~~this.props.timeOffset;
    }

    componentDidMount() {
        this.props.clockStore.subscribe(() => {
            let newDate = Moment(this.props.clockStore.getState()).add(this.props.timeOffset, 'hours').toDate();
            this.setState({
                date: newDate
            });

            this.rotateArrows();
        });
    }

    rotateArrows() {
        let rootElement = ReactDOM.findDOMNode(this);

        let arrowHours = rootElement.getElementsByClassName('arrow-hours');
        arrowHours = arrowHours[0];

        let arrowMinutes = rootElement.getElementsByClassName('arrow-minutes');
        arrowMinutes = arrowMinutes[0];

        let arrowSeconds = rootElement.getElementsByClassName('arrow-seconds');
        arrowSeconds = arrowSeconds[0];

        this.rotateArrow(arrowSeconds, 6 * this.state.date.getSeconds());
        this.rotateArrow(arrowMinutes, 6 * this.state.date.getMinutes());
        this.rotateArrow(arrowHours, 30 * (this.state.date.getHours() % 12) + this.state.date.getMinutes() / 2);
    }

    rotateArrow(element, degrees) {
        element.setAttribute('transform', 'rotate('+ degrees +' 50 50)')
    }

    render() {
        return (
            <div className={ 'clock-wrapper' }>
                <div>Смещение: {this.props.timeOffset}</div>
                <div>Время: { Moment(this.state.date).format('DD.MM.YYYY HH:mm:ss') }</div>
                <svg className={ 'clock' } viewBox="0 0 100 100">
                    <circle className={ 'clock-face' } cx="50" cy="50" r="45"/>
                    <g className={ 'arrows' }>
                        <rect className={ 'arrow-hours' } x="48.5" y="12.5" width="3" height="40" rx="2.5" ry="2.55" />
                        <rect className={ 'arrow-minutes' } x="48" y="12.5" width="3" height="40" rx="2" ry="2"/>
                        <line className={ 'arrow-seconds' } x1="50" y1="50" x2="50" y2="16" />
                    </g>
                </svg>
            </div>
        );
    }

}

HotelClock.defaultProps = {
    timeOffset: '0'
};

export default HotelClock;