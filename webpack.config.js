const path = require("path");

module.exports = {
    entry: "./dev/js/index.jsx",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "assets/js/")
    },
    module: {
        rules: [
            {
                test: /.js$/,
                loader: "babel-loader",
                exclude: /node_modules/
            },
            {
                test : /\.jsx?/,
                include : __dirname,
                loader : 'babel-loader'
            }
        ]
    }
};
